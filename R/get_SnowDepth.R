#' Download SNODAS Snow Depth
#'
#' Downloads daily subsets of masked Snow Depth provided by the Snow Data Assimilation System (SNODAS), see: \url{https://nsidc.org/data/g02158}.
#'
#' @param startDate A character with the starting date of interest. ISO date format is required (e.g. YYYY-mm-dd)
#' @param endDate A character with the ending date of interest. ISO date format is required (e.g. YYYY-mm-dd). If NULL, only the single start date file will be downloaded.
#' @param outDir A character specifying the root output directory for downloaded files. This should be the root directory, sub-directories will be created if missing. Assumes your current working directory by default
#' @param crop.extent A raster or polygon (\code{sf} object) used to crop the resulting dataset(s) from the source extent of the SNODAS data
#' @param output.crs An EPSG code representing the desired CRS of the output raster. Must be a valid EPSG integer. If NULL (default), SNODAS source CRS will be used (EPSG:4326)
#' @param res single or (vector of) two numerics to set the output resolution. Must not be NULL if you set \code{crop.extent} to an \code{sf} or \code{raster} object.
#'
#' @return Returns the downloaded raw SNODAS tar files and saves to disk a GeoTiff of Snow Depth for each requested day. Output will be cropped and reprojected if specified.
#'
#' @examples
#' library(DEVISER)
#' data(devise_studyarea)
#' # Download a set of SNODAS Snow Depth files, using the current working directory as the beginning of file storage, do not crop to an extent.
#' get_SnowDepth(startDate = "2005-10-01", endDate = "2005-10-02", output.crs = 5072, res = c(1000, 1000))
#'
#' @export

get_SnowDepth <- function(startDate, endDate = NULL, outDir = NULL, crop.extent = NULL, output.crs = NULL, res = NULL){
  # Check argument validity
  # Start date
  if(!exists("startDate")){
    stop("startDate must be a charater string of a date (e.g. '2020-06-30')")
  } else{
    if(any(is.null(startDate), is.na(startDate))){
      stop("startDate must be a charater string of a date (e.g. '2020-06-30')")
    }
  }
  if(!is.character(startDate)) stop("startDate must be a charater string of a date (e.g. '2020-06-30')")
  if(!lubridate::is.Date(as.Date(startDate))) stop("startDate must be a charater string of a date (e.g. '2020-06-30')")
  # End date
  if(!is.null(endDate)){
    if(!is.character(endDate)) stop("endDate must be a charater string of a date (e.g. '2020-06-30')")
    if(inherits(try(lubridate::is.Date(as.Date(endDate)), silent = TRUE), "try-error")) stop("endDate must be a charater string of a date (e.g. '2020-06-30')")
  }

  # output directory
  if(!is.null(outDir)){
    if(!is.character(outDir)) stop("outDir must be a character string. Preferably of a directory that exists within your file system.")
    if(!dir.exists(outDir)) stop("You have specified a directory that does not currently exist within your file system!")
  }

  # extent object
  if(!is.null(crop.extent)){
    if(!any(class(crop.extent) %in% c("sf", "RasterLayer"))) stop("crop.extent must be of class 'sf' or 'raster'")
  }

  # CRS object
  if(!is.null(output.crs)){
    if(!output.crs %% 1 == 0) stop("output.crs must be a valid EPSG code, which is an integer!")
    if(is.na(sf::st_crs(output.crs)$wkt)) stop("output.crs is not a valid EPGS code!")
    # Check to make sure the res isn't NULL as well
    if(is.null(res)) stop("You specified an output.crs, but left res = NULL. This is not allowed. Please specify an output resolution (e.g. for a projected CRS res = c(1000, 1000) would be appropriate).")
  }

  # ftp site of snodas data
  url <- "ftp://sidads.colorado.edu/DATASETS/NOAA/G02158/masked/"

  # Deal with creating directories for output
  if(is.null(outDir)){
    # set the outDir variable
    outDir<- getwd()
  }

  # First check to make sure last character of outDir does not contain slashes of any sort
  outDir<- paste0(strsplit(outDir, "\\\\")[[1]], collapse = "/")
  outDir<- paste0(strsplit(outDir, "/")[[1]], collapse = "/")

  if(!dir.exists(paste0(outDir, "/SNODAS"))){
    dir.create(paste0(outDir, "/SNODAS"))
  }
  # Create RAW directory if it doesn't exist
  ## DONT NEED
  if(!dir.exists(paste0(outDir, "/SNODAS/RawSNODAS"))){
    dir.create(paste0(outDir, "/SNODAS/RawSNODAS"))
  }
  # Create gzip directory if it doesn't exist
  if(!dir.exists(paste0(outDir, "/SNODAS/gzSNODAS"))){
    dir.create(paste0(outDir, "/SNODAS/gzSNODAS"))
  }
  # Create Snow Depth directory if it doesn't exist
  if(!dir.exists(paste0(outDir, "/SNODAS/SNODAS_SnowDepth"))){
    dir.create(paste0(outDir, "/SNODAS/SNODAS_SnowDepth"))
  }


  # Process the dates, set up a sequence
  # Create a list of dates that were requested
  ifelse(!is.null(endDate), dts<- seq(as.Date(startDate), as.Date(endDate), by = "day"), dts<- as.Date(startDate))

  # Given the list of dates, loop through each one and grab the proper dataset from the FTP, extract, then crop to extent
  for(i in 1:length(dts)){
    # Get the year
    yr<- lubridate::year(dts[i])

    # Get the month as character
    mc<- as.character(lubridate::month(dts[i], label = TRUE, abbr = TRUE))

    # Get month as number with a leading 0 if necessary
    mn<- stringr::str_pad(lubridate::month(dts[i]), width = 2, side = "left", pad = "0")

    # Get the day with a leading 0
    d<- stringr::str_pad(lubridate::day(dts[i]), width = 2, side = "left", pad = "0")

    # Test if file has already been downloaded
    if(!file.exists(paste(outDir, paste("/SNODAS/RawSNODAS/SNODAS_", yr, mn, d, ".tar", sep = ""), sep = ""))){
      # Now get the .tar file from server, sometimes they are missing, so use a try
      tst<- try(utils::download.file(paste(paste(paste(url, yr, sep = ""), paste(mn, mc, sep = "_"), sep = "/"), paste("/SNODAS_", yr, mn, d, sep = ""), ".tar", sep = ""),
                                     paste(outDir, paste("/SNODAS/RawSNODAS/SNODAS_", yr, mn, d, ".tar", sep = ""), sep = ""), quiet = TRUE, mode = "wb"), silent = TRUE)

    } # Close if SNODAS Raw Exists

    # if the file exists, we can do some processing
    if(file.exists(paste(outDir, paste("/SNODAS/RawSNODAS/SNODAS_", yr, mn, d, ".tar", sep = ""), sep = ""))){
      # Get out of tar format
      utils::untar(paste(outDir, paste("/SNODAS/RawSNODAS/SNODAS_", yr, mn, d, ".tar", sep = ""), sep = ""), list = FALSE, exdir = paste0(outDir, "/SNODAS/gzSNODAS"), verbose=FALSE)
      # Get a list of dat files.
      sfiles<- dir(paste0(outDir, "/SNODAS/gzSNODAS"), pattern = ".gz$", full.names = TRUE)
      sfiles<- sfiles[grep(pattern = ".dat.gz$", sfiles)]
      # Reduce to the Snow Depth layer
      sfiles<- sfiles[grep("v11036", sfiles)]

      # Unzip
      sfiles<- R.utils::gunzip(sfiles[length(sfiles)])

      # Must read in the data from a binary format. Rescale by dividing by 1000
      r <- readBin(sfiles, "integer", n = 6935 * 3351, size = 2, signed = TRUE, endian = "big")

      # deal with NoData cells
      r[r == -9999] <- NA

      # Rescale according to metadata
      r <- r / 1000

      # Coerce the vector to a matrix, then to a raster, this will be a 1km cell in DecDeg
      r <- terra::rast(matrix(r, nrow = 3351, ncol = 6935, byrow = TRUE), crs = "epsg:4326", extent = terra::ext(c(-124.733333333328, -66.9416666666640, 24.9499999999990, 52.8749999999979)))

      # Crop the raster to the input extent if provided
      if(!is.null(crop.extent)){
        if(any(class(crop.extent) %in% "sf")){
          # reproject the sf object to match the SNODAS
          ext<- sf::st_transform(crop.extent, crs = 4326)
          ext <- as(ext, "Spatial")
          r<- terra::crop(r, terra::vect(ext))
        }
        if(any(class(crop.extent) %in% "RasterLayer")){
          ext<- terra::project(crop.extent, crs = "epsg:4326")
          r<- terra::crop(r, ext)
        }
      }

      # Reproject raster to the CRS specified
      if(!is.null(output.crs)){
        r<- terra::project(r, res = res, y = paste0("epsg:", output.crs))
      }

      # Replace values less than 0, have to do this after reprojection due to bilinear interpolation
      r[r < 0] <- NA

      # Save to the correct directory.
      terra::writeRaster(r, paste0(outDir, "/SNODAS/SNODAS_SnowDepth", paste0("/SNODAS_", yr, mn, d, "_SnowDepth.tif")), filetype = "GTiff", overwrite = TRUE)

      # Remove our mess from the file directory
      file.remove(dir(paste0(outDir, "/SNODAS/gzSNODAS"), pattern = ".gz$", full.names = TRUE))
      file.remove(dir(paste0(outDir, "/SNODAS/gzSNODAS"), pattern = ".dat$", full.names = TRUE))

    } # close tar exists

    # Print the progress
    print(paste("Finished processing day:", dts[i], Sys.time()))
    Sys.sleep(0.01)
  } # Close i loop

}


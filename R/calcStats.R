# Internal function
calcStats <- function(df, stats){
  # d<- dplyr::filter(df, Year == mn$Year[j] & Month == mn$Month[j])
  # Stars package method
  rs<- terra::rast(df$Files)
  #rs<- stars::st_redimension(rs)
  if(stats == "Max"){
    #rs<- stars::st_apply(rs, 1:2, function(x) quantile(x, probs = 0.975, na.rm = TRUE))
    rs<- terra::app(rs, fun = function(x) quantile(x, probs = 0.975, na.rm = TRUE))
  }
  if(stats == "Mean"){
    #rs<- stars::st_apply(rs, 1:2, function(x) mean(x, na.rm = TRUE))
    rs<- terra::app(rs, fun = function(x) mean(x, na.rm = TRUE))
  }
  if(stats == "Median"){
    #rs<- stars::st_apply(rs, 1:2, function(x) median(x, na.rm = TRUE))
    rs<- terra::app(rs, fun = function(x) median(x, na.rm = TRUE))
  }
  if(stats == "Sum"){
    rs<- terra::app(rs, fun = function(x) sum(x, na.rm = TRUE))
  }
  return(rs)
}